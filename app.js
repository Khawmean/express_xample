require('dotenv').config()
const express = require('express');
const swaggerSpecs = require('./config/swagger');
swaggerUi = require("swagger-ui-express");
var app = express()

// swagger config
app.use("/docs", swaggerUi.serve,  swaggerUi.setup(swaggerSpecs));

// static file
app.use('/static', express.static('public'));

//json
app.use(express.json())

// set view engine
app.set('view engine', 'pug');
app.set('views','./views');

// Routes
app.use(require('./routes/index'))
app.use(require('./routes/login'))
app.use(require('./routes/post'))
app.use(require('./routes/view/view'))
app.use(require('./routes/view/login'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    // next(err);
    res.render('page/notFound')
  });

app.listen(process.env.PORT)