class Post {
    constructor(id, title, description, userId, date, image){
        this.id = id
        this.title = title
        this.description = description
        this.userId = userId
        this.date = date
        this.image = image
    }
}


module.exports = Post