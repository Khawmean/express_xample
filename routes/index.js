var express = require('express');
const auth = require('../config/auth');
const db = require('../config/db');
var router = express.Router();
db.connect()
/* GET home page. */
/**
 * @swagger
 * paths:
 *   /:
 *    get:
 *      description: user for login
 *      tags: [Home]
 *      responses:
 *        '200':
 * components:
 *    responses:
 *      UnauthorizedError:
 *        description: Access token is missing or invalid
 *
 *        
 *    
*/
router.get('/',auth.admin, (req, res)=>{

  res.send('home')

});



module.exports = router;
