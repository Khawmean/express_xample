var express = require('express');
const db = require('../../config/db');

var viewRouter = express.Router();

/* GET home page. */
viewRouter.get('/home', (req, res)=>{

  var data;
  db.query('SELECT * FROM users', function (err, rows, fields) {
    if (err) return err
  
    data = rows
    res.render('index',{"data" : data});
  })

});

viewRouter.get('/view/login', (req, res)=>{

    res.render('page/login')
  
});



module.exports = viewRouter;