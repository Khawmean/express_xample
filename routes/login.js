var express = require('express');
const auth = require('../config/auth');
const db = require('../config/db');
const jwt = require('jsonwebtoken')
var loginRoute = express.Router();
var bodyParser = require('body-parser')

/* GET Login. */
/**
 * @swagger
 * /login:
 *  post:
 *    description: user for login
 *    tags: [User]
 *    responses:
 *      '200':
 *        description: ok
 *    requestBody:
 *       content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                username:
 *                  type: string
 *                password:
 *                  type: string
 *       required: true
 *       description: okay
 *    
*/
loginRoute.post('/login', (req, res)=>{

  const username = req.body.username
  const password = req.body.password
  db.query(`SELECT * FROM users WHERE username LIKE '${username}' and password LIKE '${password}'`, 
  function (err, result, fields) {
    // if (err) throw err;
  
    if(result[0]!=undefined){
      const user = {name: username, password: password, role: result[0].role}
      const asseccToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
      res.json({token : asseccToken})
    }else{
      res.json({message: "incorrect username or password"})
    }
    
  })

});


// Register //
/**
 * @swagger
 * /register:
 *  post:
 *    description: user for register new account
 *    tags: [User]
 *    responses:
 *      '200':
 *        description: ok
 *    requestBody:
 *       content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                username:
 *                  type: string
 *                password:
 *                  type: string
 *       required: true
 *       description: okay
 *    
*/
loginRoute.post('/register', (req, res)=>{

  const username = req.body.username
  const password = req.body.password

  db.query(`INSERT INTO users ( username, password, role, status) VALUES ('${username}', '${password}', 'USER', '1')`, 
  function (err, result) {
    
    if (err || err != null) return res.json({message: "Username already exist"});
    res.json({message: "Register successfully"})
    
  })

});


module.exports = loginRoute;