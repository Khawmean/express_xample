var express = require('express');
const auth = require('../config/auth');
const db = require('../config/db');
var postRoute = express.Router();
var BaseRes = require('../models/Response')

/* Get all posts */
/**
 * @swagger
 * paths:
 *   /posts:
 *    get:
 *      description: user for login
 *      tags: [Post]
 *      responses:
 *        '200':
 *          description: ok
 *      parameters:
 *      - name: id
 *        in: query  
 *        schema:
 *           type: number
 *      - name: title
 *        in: query  
 *        schema:
 *        type: String
*/
postRoute.get('/posts', (req, res)=>{
    var id = " > 0";
    var title = " NOT LIKE ''";
    if(req.query.id!=undefined){
        id = ` = ${req.query.id}`;
    }
    if(req.query.title!=undefined){
        title = ` LIKE '%${req.query.title}%'`
    }

    db.query(`SELECT * FROM posts WHERE id ${id} AND title ${title}`, (err, result)=>{
        if(err) return res.json(new BaseRes("0009", "Somthing went wrong.", "{}"))
        res.json(new BaseRes("0000", "Fetch data successfully", result))
    })
});

/* add a post */
/**
 * @swagger
 * paths:
 *   /post/save:
 *    post:
 *      description: user for login
 *      tags: [Post]
 *      responses:
 *        '200':
 *          description: ok
 *      requestBody:
 *         content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Post'
 *         required: true
 *         description: okay
*/
postRoute.post('/post/save',auth.permitAll, (req, res)=>{
  db.query(`INSERT INTO posts(title, description,user_id, date, image) VALUES ('${req.body.title}','${req.body.description}','${req.body.userId}','${req.body.date}','${req.body.image}')`, (err, result)=>{
      if(err) return res.json(new BaseRes("0009", "Somthing went wrong.", "{}"))
      res.json(new BaseRes("0000", "Save data successfully", result.insertId))
  })
});

/* update a post */
/**
 * @swagger
 * paths:
 *   /post/update:
 *    put:
 *      description: update a post
 *      tags: [Post]
 *      responses:
 *        '200':
 *          description: ok
 *      requestBody:
 *         content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Post'
 *         required: true
 *         description: okay
*/
postRoute.put('/post/update',auth.permitAll, (req, res)=>{
    db.query(`UPDATE posts SET title='${req.body.title}',description='${req.body.description}',user_id=${req.body.userId},date='${req.body.date}',image='${req.body.image}' WHERE id = ${req.body.id}`, (err, result)=>{
        if(err) return res.json(new BaseRes("0009", "Post does not exist.", null))
        res.json(new BaseRes("0000", "Update data successfully", result.message))
    })
  });


/* delete a post */
/**
 * @swagger
 * paths:
 *   /post/delete/{id}:
 *    delete:
 *      description: Delete a post
 *      tags: [Post]
 *      responses:
 *        '200':
 *          description: ok
 *      parameters:
 *      - name: id
 *        in: path  
 *        required: true
 *        schema:
 *           type: number
*/
postRoute.delete('/post/delete/:id',auth.permitAll, (req, res)=>{
    db.query(`DELETE FROM posts WHERE id = ${req.params.id}`, (err, result)=>{
        if(err || result.affectedRows==0) return res.json(new BaseRes("0009", "Post does not exist.", null))
        res.json(new BaseRes("0000", "Delete data successfully", null))
    })
  });

module.exports = postRoute;
