const jwt = require('jsonwebtoken')

var auth = {
    admin : function authenticateToken(req, res, next){
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
    
        if(token == null) return res.sendStatus(401)
        
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
            if(err || user.role != "ADMIN") return res.sendStatus(403)
            req.user = user
            next()
            
        })
    
        
    },
    user : function authenticateToken(req, res, next){
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
    
        if(token == null) return res.sendStatus(401)
        
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
            if(err || user.role != "USER") return res.sendStatus(403)
            req.user = user
            next()
        })
    
        
    },

    adminUser: function authenticateToken(req, res, next){
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
    
        if(token == null) return res.sendStatus(401)
        
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
            if(err || user.role != "ADMIN" && user.role != 'USER') return res.sendStatus(403)
            req.user = user
            next()
        })
    
        
    },

    permitAll: function authenticateToken(req, res, next){
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
    
        if(token == null) return res.sendStatus(401)
        
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
            if(err) return res.sendStatus(403)
            req.user = user
            next()
        })
    
        
    },
}




module.exports = auth