swaggerJsdoc = require("swagger-jsdoc"),
swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    components: {
      securitySchemes: {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
      schemas: {
        Post: {
          type: Object,
          properties: {
            id: {
              type: Number,
              example: "0",
            },
            title: {
              type: String,
              example: "title",
            },
            description: {
              type: String,
              example: "description",
            },
            userId: {
              type: Number,
              example: "0",
            },
            date: {
              type: String,
              example: '2022-01-15',
            },
            image: {
              type: String,
              example: "string",
            },
          },
      }
      },
    },
    security: [
      {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
    ],
    info: {
      title: "Testing express swagger docs",
      version: "1.0.5",
      description:
        "This is a simple CRUD API application made with Express and documented with Swagger",
      contact: {
        name: "Khaw",
      },
    },
    servers: [
      {
        url: "http://34.124.240.235:3000",
      },
    ],
  },
  apis: ["routes/*.js"],
};

const swaggerSpecs = swaggerJsdoc(swaggerOptions);

module.exports = swaggerSpecs;